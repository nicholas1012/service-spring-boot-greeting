/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nicholas.main.batch;

import com.nicholas.main.model.Greeting;
import com.nicholas.main.service.GreetingService;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author nicholassnyman
 */
@Component
@Profile(value = "batch")
public class GreetingBatch {

    final private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private GreetingService greetingService;

    @Scheduled(cron = "*/30 * * * * *")
    public void cronJobs() {
        logger.info("** cron job started **");

        final Collection<Greeting> greetings = greetingService.findAll();
        logger.info("The size of the greetings db"+greetings.size());

        logger.info("** cron job started **");
    }

}
