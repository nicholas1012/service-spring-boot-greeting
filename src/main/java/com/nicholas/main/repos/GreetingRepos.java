/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nicholas.main.repos;

import com.nicholas.main.model.Greeting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author nicholassnyman
 */
@Repository
public interface GreetingRepos extends JpaRepository<Greeting, Integer>{
    
}
