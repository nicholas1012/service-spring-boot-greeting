package com.nicholas.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
/**
 *
 * @author nicholassnyman
 */
@SpringBootApplication
@EnableTransactionManagement
@EnableCaching
@EnableScheduling
@EnableAsync
public class Main {

    public static final String CACHE_GREETINGS = "greetings";
    
    public static void main(String[] args) throws Exception {
        SpringApplication.run(Main.class, args);
    }
    
    @Bean
    public CacheManager cacheManager(){
        //ConcurrentMapCacheManager cacheManager = new ConcurrentMapCacheManager(CACHE_GREETINGS);
        GuavaCacheManager cacheManager = new GuavaCacheManager(CACHE_GREETINGS);
        return cacheManager;
    }
}
