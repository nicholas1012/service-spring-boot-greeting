/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nicholas.main.service;

import com.nicholas.main.Main;
import com.nicholas.main.model.Greeting;
import com.nicholas.main.repos.GreetingRepos;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author nicholassnyman
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class GreetingServiceBean implements GreetingService {

       
    @Autowired
    private GreetingRepos greetingRepos;
    
    
    @Override
    public Collection<Greeting> findAll() {
        return greetingRepos.findAll();
    }

    @Override
    @Cacheable(value = Main.CACHE_GREETINGS, key = "#id")
    public Greeting findById(Integer id) {
        return greetingRepos.findOne(id);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @CachePut(value = Main.CACHE_GREETINGS, key = "#result.id")
    public Greeting save(Greeting greeting) {
        if (greeting.getId() != null){
            return update(greeting);
        }
        
        final Greeting savedGreeting = greetingRepos.save(greeting);
        
//        if (savedGreeting.getId() == 5){
//            throw new RuntimeException("Roll me back brah!");
//        }
        return savedGreeting;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @CachePut(value = Main.CACHE_GREETINGS, key = "#greeting.id")
    public Greeting update(Greeting greeting) {
        Greeting found = greetingRepos.findOne(greeting.getId());
        if (found == null){
            return null;
        }
        return greetingRepos.save(greeting);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @CacheEvict(value = Main.CACHE_GREETINGS, key = "#id")
    public void delete(Integer id) {
        greetingRepos.delete(id);        
    }

    @Override
    @CacheEvict(value = Main.CACHE_GREETINGS, allEntries = true)
    public void evictCache() {
        
    }
    
}
