/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nicholas.main.service;

import com.nicholas.main.model.Greeting;
import java.util.Collection;

/**
 *
 * @author nicholassnyman
 */
public interface GreetingService {
    
    Collection<Greeting> findAll();
    Greeting findById(Integer id);
    Greeting save(Greeting greeting);
    Greeting update(Greeting greeting);
    void delete(Integer id);
    void evictCache();
}
